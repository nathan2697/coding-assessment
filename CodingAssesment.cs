using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
// namespace declaration 
namespace CodingAssesment
{

    // Class declaration for each row of data in data.csv
    public class Item
    {
        public int productID;
        public float quantity;
        public float unitPrice;
        public string saleTime;

        public Item(int id, float amount, float itemPrice, string saleDate) //sets each field for each column of data.csv
        {
            productID = id;
            quantity = amount;
            unitPrice = itemPrice;
            saleTime = saleDate;
        }
    }
    class Program
    {

        // Main Method 
        static void Main(string[] args)
        {
            // load data into list
            List<Item> listOfItems = initItems("data.csv");

            //each function answers each of the questions listed in code assesment
            totalRevenue(listOfItems);
            mostPopularProduct(listOfItems);
            totalQuantityAndTotalRevenue(listOfItems);
            monthWithHighestRevenue(listOfItems);

            //unit tests to be run for each function made

            //testInitItems();
            //testTotalRevenue();
            //testMostPopularProduct();
            //testTotalQuantityAndTotalRevenue();
            //testMonthWithHighestRevenue();
            Console.ReadKey();
        }


        public static List<Item> initItems(string data)
        {
            List<Item> listOfItems = new List<Item>();
            var Lines = File.ReadAllLines(data);

            //if no data was loaded from fille report null
            if (Lines.Length == 0)
            {
                Console.WriteLine("Data loaded from file was NULL");
                return listOfItems;
            }

            //otherwise store data columns into item class as desired data types
            string[] temp;
            for (int j = 1; j < Lines.Length; j++)
            {
                temp = Lines[j].Split(',');
                listOfItems.Add(new Item(Int32.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]), temp[3]));

            }
            return listOfItems;
        }


        //testsnthe case of reporting a empty file
        public static void testInitItems()
        {
            List<Item> listOfItems = initItems("emptydata.csv");
        }

        //tests the case of calculating revenue of empty list
        public static void testTotalRevenue()
        {
            List<Item> listOfItems = new List<Item>();
            totalRevenue(listOfItems);
        }


        //tests the case of calculating most popular product of empty list
        public static void testMostPopularProduct()

        {
            List<Item> listOfItems = new List<Item>();
            mostPopularProduct(listOfItems);

        }

        //tests the case of calculating total quantity and revenue of empty list

        public static void testTotalQuantityAndTotalRevenue()
        {
            List<Item> listOfItems = new List<Item>();
            totalQuantityAndTotalRevenue(listOfItems);
        }

        //tests the case of calculating highest revenue of all months

        public static void testMonthWithHighestRevenue()
        {
            List<Item> listOfItems = new List<Item>();
            monthWithHighestRevenue(listOfItems);
        }


        //parses date string and adds date to dictionary
        //when date exists only add to revenue for that data for rest of the list
        public static void monthWithHighestRevenue(List<Item> listOfItems)
        {
            Dictionary<string, float> itemMap = new Dictionary<string, float>();
            for (int i = 0; i < listOfItems.Count; i++)
            {

                if (!itemMap.ContainsKey(listOfItems[i].saleTime.Split('-')[1]))
                {
                    itemMap.Add(listOfItems[i].saleTime.Split('-')[1], listOfItems[i].unitPrice);
                }

                else
                {
                    itemMap[listOfItems[i].saleTime.Split('-')[1]] += listOfItems[i].unitPrice;
                }

            }

            if (itemMap.Count > 0)
            {
                Console.WriteLine("The month with the highest revenue: " + itemMap.Aggregate((l, r) => l.Value > r.Value ? l : r).Key);
            }

            else
            {//reports empty list 
                Console.WriteLine("Insufficient Items to calculate month with the highest revenue.");

            }


        }

        //adds product to dictionary
        //when product ID exists only add the quantity sold to existing key-value pair in dictionairy for rest of list
        public static void mostPopularProduct(List<Item> listOfItems)
        {

            Dictionary<int, float> itemMap = new Dictionary<int, float>();
            for (int i = 0; i < listOfItems.Count; i++)
            {

                if (!itemMap.ContainsKey(listOfItems[i].productID))
                {
                    itemMap.Add(listOfItems[i].productID, listOfItems[i].quantity);
                }

                else
                {
                    itemMap[listOfItems[i].productID] += listOfItems[i].quantity;
                }
            }

            if (itemMap.Count > 0)
            {
                Console.WriteLine("The ID of the most popular product based on quantity sold: " + itemMap.Aggregate((l, r) => l.Value > r.Value ? l : r).Key);
            }
            else
            {//reports empty list 
                Console.WriteLine("Insufficient Items to calculate most popular product");

            }
        }

        //adds product to dictionary, with list as value holding quantity and price spent
        //when product ID exists only add to the existing quantity sold and revenue for that product for existing key-value pair in dictionairy for rest of list
        public static void totalQuantityAndTotalRevenue(List<Item> listOfItems)
        {
            Dictionary<int, List<float>> itemMap = new Dictionary<int, List<float>>();
            for (int i = 0; i < listOfItems.Count; i++)
            {

                if (!itemMap.ContainsKey(listOfItems[i].productID))
                {
                    List<float> quantityAndRevenue = new List<float>();
                    quantityAndRevenue.Add(listOfItems[i].quantity);
                    quantityAndRevenue.Add(listOfItems[i].unitPrice);
                    itemMap.Add(listOfItems[i].productID, quantityAndRevenue);
                }

                else
                {
                    itemMap[listOfItems[i].productID][0] += listOfItems[i].quantity;
                    itemMap[listOfItems[i].productID][1] += listOfItems[i].unitPrice;

                }
            }

            if (itemMap.Count > 0)
            {

                foreach (var item in itemMap)
                {
                    Console.WriteLine("Product: " + item.Key + " has a total quantity of " + item.Value[0] + " sold and total revenue of " + item.Value[1]);

                }
            }

            else
            { //reports empty list 
                Console.WriteLine("Insufficient Items to calculate product total quantity sold and their revenue");

            }

        }

        //adds the price of each sale together for total revenue
        public static void totalRevenue(List<Item> listOfItems)
        {
            float totalRevenue = 0;

            for (int i = 0; i < listOfItems.Count; i++)
            {
                totalRevenue += listOfItems[i].unitPrice;
            }

            if (totalRevenue > 0)
            {
                Console.WriteLine("Total revenue for all products sold: " + totalRevenue);

            }

            else
            { //report empty list 
                Console.WriteLine("Insufficient Items sold to calculate a total revenue");
            }

        }

    }
}


